"""The core module of the conflict package"""

def capitalize_strings(strings):
    """Return caputalized version of strings"""
    return [string.title() for string in strings]
